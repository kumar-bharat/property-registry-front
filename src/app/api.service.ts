import { environment } from './../environments/environment';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import { CONST } from './app.constant';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  constructor(
    private http: HttpClient,
  ) { }

  setHeaders(isToken) {
    if (isToken) {
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `bearer ${localStorage.getItem(CONST.AUTH_TOKEN)}`
      });
    } else {
      this.headers = new HttpHeaders();
    }
  }

  filterResponse(res, keyArray) {
    if (_.isEmpty(res.Data)) {
      return res;
    } else if (!_.isEmpty(res.Data) && _.isEmpty(keyArray)) {
      return res;
    } else {
      return _.map([...res.Data], (item) => {
        return _.pick({ ...item }, keyArray);
      });
    }
  }

  getQueryStringFromObject(obj) {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  }

  postApi(endPoint, data, isTokenRequired?, queryParam?) {
    this.setHeaders(isTokenRequired);
    let url;
    if (!_.isEmpty(queryParam)) {
      const queryParamString = this.getQueryStringFromObject(queryParam);
      url = `${endPoint}?${queryParamString}`;
    } else {
      url = endPoint;
    }
    data = data ? data : {};
    const options =
      !_.isEmpty(queryParam) && queryParam.id
        ? { headers: this.headers, params: { id: queryParam.id } }
        : { headers: this.headers };
    const method = !_.isEmpty(queryParam) && queryParam.id ? 'put' : 'post';
    return this.http[method](`${environment.SERVER_URL}/${url}`, data, options);
  }

  putApi(endPoint, data, isTokenRequired?, queryParam?) {
    this.setHeaders(isTokenRequired);
    let url;
    if (!_.isEmpty(queryParam)) {
      const queryParamString = this.getQueryStringFromObject(queryParam);
      url = `${endPoint}?${queryParamString}`;
    } else {
      url = endPoint;
    }
    data = data ? data : {};
    const options =
      !_.isEmpty(queryParam) && queryParam.id
        ? { headers: this.headers, params: { id: queryParam.id } }
        : { headers: this.headers };
    return this.http.put(`${environment.SERVER_URL}/${url}`, data, options);
  }

  getApi(endPoint, queryParam?, isTokenRequired?, keyArray?) {
    this.setHeaders(isTokenRequired);
    let url;
    if (!_.isEmpty(queryParam)) {
      const queryParamString = this.getQueryStringFromObject(queryParam);
      url = `${endPoint}?${queryParamString}`;
    } else {
      url = endPoint;
    }
    return this.http.get(`${environment.SERVER_URL}/${url}`, { headers: this.headers }).pipe(
      map((res: any) => {
        return this.filterResponse(res, keyArray);
      })
    );
  }

  deleteApi(endPoint, queryParam?, isTokenRequired?) {
    this.setHeaders(isTokenRequired);
    let url;
    if (!_.isEmpty(queryParam)) {
      const queryParamString = this.getQueryStringFromObject(queryParam);
      url = `${endPoint}?${queryParamString}`;
    } else {
      url = endPoint;
    }
    return this.http.delete(`${environment.SERVER_URL}/${url}`, { headers: this.headers, params: {
      id : queryParam.id
    } });
  }
}
