import { CONST } from './../app.constant';
import { ToasterService } from './../toaster.service';
import { CommonService } from './../common.service';
import { API_ENDPOINTS } from './../api.endpoint';
import { ApiService } from './../api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-signin-signup',
  templateUrl: './signin-signup.component.html',
  styleUrls: ['./signin-signup.component.scss'],
})
export class SigninSignupComponent implements OnInit {
  @ViewChild('signInSignUpForm') signInSignUpFormController: NgForm;
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private cs: CommonService,
    private spinner: NgxSpinnerService,
    private toaster: ToasterService
  ) {
    this.route.queryParamMap.subscribe((res) => {
      if (res.get('type') === 'signin') {
        this.isRegistered = true;
        this.forgotPassword = false;
      } else if (res.get('type') === 'signup') {
        this.isRegistered = false;
      } else if (res.get('type') === 'change-password') {
        this.isRegistered = true;
        this.forgotPassword = true;
      } 
      this.pathToMove = res.get('from') ? res.get('from') : ''
      if(this.pathToMove){
        this.toaster.showSuccess('Please Login First')
      }
    });
  }
  pathToMove: string
  userName: string;
  mobileNo: number;
  email: string;
  userPassword: string;
  isRegistered = true;
  forgotPassword = false;
  userConfirmPassword:string = '';
  ngOnInit(): void {
    this.cs.scrollToTop()
  }
  validateForm() {
    let valid = true
    if (this.isRegistered && !this.forgotPassword) { /* Sign In Validation */
      
    } else if (!this.isRegistered && !this.forgotPassword) { /* Sign up Validation */
      
    } else if(this.isRegistered && this.forgotPassword) { /* Change Password Validation */
      if(this.userPassword !== this.userConfirmPassword){
        valid = false;
        this.toaster.showError('New Password and Confirm Password should be Same')
      }
    }
    return valid
  }

  /* Form Submittion */
  submit() {
    if (this.isRegistered && !this.forgotPassword) {
      this.login();
    } else if (!this.isRegistered && !this.forgotPassword) {
      this.signup();
    } else if(this.isRegistered && this.forgotPassword) {
      this.changePassword();
    }
  }

  /* New User Registration */
  signup() {
    this.spinner.show();
    const data = {
      name: this.userName,
      mobileNo: this.mobileNo,
      email: this.email,
      password: this.userPassword,
    };
    this.apiService.postApi(API_ENDPOINTS.SIGN_UP, data).subscribe(
      (res) => {
        this.isRegistered = true;
        this.forgotPassword = false;
        this.toaster.showSuccess('TOASTER.SIGNUP_SUCCESS_MSG');
        this.signInSignUpFormController.resetForm();
        this.spinner.hide();
      },
      (err) => {
        this.toaster.showError('TOASTER.ERROR_MSG');
        console.log(err);
        this.spinner.hide();
      }
    );
  }

  /* Registered Customer Login */
  login() {
    this.spinner.show();
    const data = {
      mobileNo: this.mobileNo,
      password: this.userPassword,
    };
    this.apiService.postApi(API_ENDPOINTS.LOGIN, data).subscribe(
      (res: any) => {
        if(res.accessToken){
          localStorage.setItem(CONST.AUTH_TOKEN, res.accessToken);
          this.cs.isLogin = true;
          this.cs.getUserInfo();
          this.toaster.showSuccess('TOASTER.LOGIN_SUCCESS_MSG');
          this.spinner.hide();
          if(this.pathToMove){
            this.router.navigate([`${this.pathToMove}`]);
          } else {
            this.router.navigate(['home']);
          }
        }
      },
      (err) => {
        this.toaster.showError(err.error.message);
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  /* Forgot Password */
  changePassword() {
    this.spinner.show()
    const payload = {
      password: this.userPassword,
    }
    this.apiService.putApi(API_ENDPOINTS.USER, payload, true).subscribe(
      (res) => {
        this.spinner.hide();
        this.toaster.showSuccess('Password Changed Successfully')
        localStorage.removeItem(CONST.AUTH_TOKEN);
        this.cs.isLogin = false;
        this.router.navigate(['signin-signup'], {queryParams: { type: 'signin' }});
      },
      (err) => {
        this.toaster.showError(err.error.message);
        this.spinner.hide();
        console.log(err);
      }
    );
  }
}
