import { LazyTransModule } from './../lazy-trans.module';
import { SharedModule } from './../shared.module';
import { NgModule } from '@angular/core';
import { SigninSignupRoutingModule } from './signin-signup-routing.module';
import { SigninSignupComponent } from './signin-signup.component';


@NgModule({
  declarations: [SigninSignupComponent],
  imports: [
    SigninSignupRoutingModule,
    SharedModule,
    LazyTransModule
  ]
})
export class SigninSignupModule { }
