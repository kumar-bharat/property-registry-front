import { CONST } from './../app.constant';
import { ToasterService } from './../toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from './../api.service';
import { CommonService } from './../common.service';
import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
} from '@angular/core';
import { AddDocumentComponent } from '../add-document/add-document.component';
import { API_ENDPOINTS } from '../api.endpoint';
import * as jsPDF from 'jspdf'
import htmlToPdfmake from "html-to-pdfmake";
import * as pdfMake from "pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts"
@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss'],
})
export class DocumentListComponent implements OnInit {
  @ViewChild('docAddContainerRef', { read: ViewContainerRef })
  docAddContainerRef: ViewContainerRef;
  addDocRef: any;
  documentList: any[] = [];
  base64String: any;
  constructor(
    private cs: CommonService,
    private resolver: ComponentFactoryResolver,
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private toaster: ToasterService
  ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs
  }

  ngOnInit(): void {
    this.getDocumentList();
  }

  addNewDocument(item?) {
    this.cs.loadModalDynamically(
      this,
      'docAddContainerRef',
      'addDocRef',
      AddDocumentComponent,
      (res) => {
        this.getDocumentList();
      },
      item
    );
  }

  getDocumentList() {
    this.spinner.show();
    this.apiService.getApi(API_ENDPOINTS.PROPERTY_DOCUMENT, {}, true).subscribe(
      (res) => {
        this.documentList = [...res];
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  deleteDocument(item, index) {
    const query = {
      id: item._id,
    };
    this.apiService
      .deleteApi(API_ENDPOINTS.PROPERTY_DOCUMENT, query, true)
      .subscribe(
        (res) => {
          this.documentList.splice(index, 1);
          this.toaster.showSuccess(CONST.DELETE_SUCCESS_MSG);
        },
        (err) => {
          this.toaster.showError(CONST.ERROR_MSG);
        }
      );
  }

  printDocument() {
    // const pdf = new jsPDF({
    //   orientation: 'p',
    //   unit: 'pt',
    //   format: 'legal',
    //   putOnlyUsedFonts: true,
    //   floatPrecision: 16, // or "smart", default is 16
    // });
    let printContents;
    let popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    const content = `<html><head><title>Print tab</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"
    />
    <link
    href="../../assets/css/print.css"
    rel="stylesheet"
    type="text/css"/>
    </head><body onload="">${printContents}</body></html>`;

    // pdf.fromHTML(
    //   content,
    //   15,
    //   15,
    //   {
    //     width: 180, elementHandlers: ''
    //   });
    // pdf.save('kkk');
    // const d = pdf.output('datauristring');
    // this.base64String = d.split(',')[1];
    // printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(content);
    popupWin.document.close();
    popupWin.focus();
    setTimeout(() => {
      // popupWin.print();
      // popupWin.close();
    }, 1000);
  }

  openPDF() {
    const d = `<html>
    <head>
    <title>Habibi</title>
    <link
    rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
    crossorigin="anonymous"
  />
  <link
  href="../../assets/css/print.css"
  rel="stylesheet"
  type="text/css"/>
    <style>.text-danger{color: red;text-align: center;}</style>
    </head>
    <body>
    ${document.getElementById('print-section').innerHTML}
    </body>
    </html>`;
    // let DATA = document.getElementById('testPdf');
    let doc = new jsPDF("p", "pt", "a4");
    doc.fromHTML(d, 15, 15);
    doc.output("dataurlnewwindow");
  }

  convertHtmlToPdf(){
    pdfMake.fonts = {
      ...pdfMake.fonts,
      Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-MediumItalic.ttf'
    },
      Mangal: {
        normal: '../../assets/fonts/MangalRegular.ttf',
        bold: '../../assets/fonts/MangalBold.ttf',
        italics: '../../assets/fonts/MangalBold.ttf',
        bolditalics: '../../assets/fonts/MangalBold.ttf'
     }
   }
   const htmlToConvert = `${document.getElementById('print-section').innerHTML}`
    let val = ''
    setTimeout(() => {
    const  val = htmlToPdfmake(htmlToConvert);      
    }, 5000);
    const docDefinition = {content:val,
      defaultStyle: {
        font: 'Mangal'
    }
  };
    pdfMake.createPdf(docDefinition).open();
  }
}
