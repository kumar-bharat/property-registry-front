import { LazyTransModule } from './../lazy-trans.module';
import { SharedModule } from './../shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentListRoutingModule } from './document-list-routing.module';
import { DocumentListComponent } from './document-list.component';
import { AddDocumentComponent } from '../add-document/add-document.component';


@NgModule({
  declarations: [DocumentListComponent, AddDocumentComponent],
  imports: [
    CommonModule,
    DocumentListRoutingModule,
    SharedModule,
    LazyTransModule
  ],
  entryComponents: [AddDocumentComponent]
})
export class DocumentListModule { }
