import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime'
import { ArchwizardModule } from 'angular-archwizard';
import { HeaderComponent } from '../app/app-start/header/header.component';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
  CommonModule,
    FormsModule,
    NgSelectModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      closeButton: true,
      // disableTimeOut: true,
      // autoDismiss: false,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgxSpinnerModule,
    FlexLayoutModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    ArchwizardModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    CommonModule,
    FormsModule,
    NgSelectModule,
    HttpClientModule,
    ToastrModule,
    NgxSpinnerModule,
    FlexLayoutModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    ArchwizardModule,
    RouterModule
  ]
})
export class SharedModule { }
