import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class ToasterService {
  transalteObj: any = {};
  defaultLang = 'hi';
  constructor(
    private toastr: ToastrService,
    private translate: TranslateService
  ) {
    this.translate.getTranslation(this.defaultLang).subscribe((res) => {
      this.transalteObj = res;
    });
  }

  showSuccess(title, message?) {
    this.translate.get(title).subscribe((res) => {
      this.toastr.success(res, message);
    });
  }

  showError(title, message?) {
    this.translate.get(title).subscribe((res) => {
      this.toastr.error(res, message);
    });
  }

  showInfo(title, message?) {
    this.translate.get(title).subscribe((res) => {
      this.toastr.info(res, message);
    });
  }

  showWarning(title, message?) {
    this.translate.get(title).subscribe((res) => {
      this.toastr.warning(res, message);
    });
  }
}
