import { ToasterService } from './toaster.service';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'prope-registry';
  langs = ['en', 'hi', 'fr'];
  constructor(public translateService: TranslateService, private toaster: ToasterService) {
    // const browserlang = this.translateService.getBrowserLang();
    // if (this.langs.indexOf(browserlang) > -1) {
    //   this.translateService.setDefaultLang(browserlang);
    // } else {
      this.toaster.defaultLang = 'hi';
      this.translateService.setDefaultLang(this.toaster.defaultLang);
    // }
  }
}
