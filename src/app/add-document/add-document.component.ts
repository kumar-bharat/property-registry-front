import { CommonService } from './../common.service';
import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from '../api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from '../toaster.service';
import { API_ENDPOINTS } from '../api.endpoint';
import * as _ from 'lodash';
import { MovingDirection } from 'angular-archwizard';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.scss'],
})
export class AddDocumentComponent implements OnInit, OnDestroy {
  @Output() closeModal = new EventEmitter();
  @ViewChild('documentTypeDetailForm') documentTypeDetailFormControl: NgForm;
  @ViewChild('propertyDetailFrom') propertyDetailFrom: NgForm;
  @ViewChild('partyDetailForm') partyDetailForm: NgForm;
  userType: number;
  mobileNo: number;
  panNo: any;
  aadharNo: number;
  partyName: string;
  address: string;
  cast: string;
  age: string;
  fatherName: string;
  partyDetailsData: any[] = [];
  partyDetailEditIndex: any;
  villageName: string;
  jamabandiSamvat: string;
  namantranSankhya: string;
  visheshTippadi: string;
  patwarMandal: string;
  upTahsil: string;
  tahsil: string;
  district: string;
  amountTypeId: any;
  documentTypeId: any;
  unitOfAreaId: any;
  upPanjiyakKaryalay: any;
  pratifalNagadRashi: any;
  documentLevelId: any;
  selectedPratifalRashiTypeId: any;
  checkNo: any;
  bankName: string;
  checkAmountData: any[] = [
    {
      paymentType: null,
      checkNo: null,
      bankName: null,
      amount: null,
    },
  ];
  stampVendorName: string;
  stampVendorAddress: string;
  stampVendorLicenseNo: number;
  stampRegisterNo: number;
  stampDate: any;
  stampData: any[] = [
    {
      id: 0,
      stampRate: null,
      stampCount: null,
      stampSerialNo: null,
      stampAmount: null,
    },
  ];
  cashAmount: number;
  propertyMeasurementParameters: any[] = [
    {
      accountNo: null,
      area: null,
      bhoomiKisma: null,
      khasraNo: null,
      bhoomiHissaType: null,
      disha: null,
      vikrayKshetrafal: null
    },
  ];
  editPropertyIndex = null;
  propertyDetailsData: any[] = [];
  documentNo: any;
  relation: number;
  hastantaritHissa: number;
  bhoomiHissa: number;
  currentPartyBhoomiHissa: any;

  /* Conditions ngmodals */
  distanceFromAabadiArea: number;
  distanceFromKhananArea: number;
  distanceFromIndustrialArea: number;
  nameOfNearbyRoad: string;
  distanceFromNearbyRoad: number;
  constructionAreaMeasurement: any;
  landTypeId: number;
  mudrankTypeId: number;
  editPropertyId: any;
  extraaConditions: string[] = [''];
  constructor(
    public cs: CommonService,
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private toaster: ToasterService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.closePopUp();
  }
  addMeasureField() {
    const data = {
      accountNo: null,
      area: null,
      bhoomiKisma: null,
      khasraNo: null,
      bhoomiHissaType: null,
      disha: null,
      vikrayKshetrafal: null
    };
    this.propertyMeasurementParameters.push(data);
  }

  removeMeasureField(index) {
    this.propertyMeasurementParameters.splice(index, 1);
  }

  openModal(data) {
    this.cs.openModal('add_new_doc');
    if (!_.isEmpty(data)) {
      this.assignFormData(data);
    }
  }

  closePopUp(data?) {
    this.cs.closeModal('add_new_doc');
    this.closeModal.emit(data);
  }

  addCheckField() {
    const data = {
      paymentType: null,
      checkNo: null,
      bankName: null,
      amount: null,
    };
    this.checkAmountData.push(data);
  }

  removeCheckField(index) {
    this.checkAmountData.splice(index, 1);
  }

  addStampField() {
    const data = {
      id: 0,
      stampRate: null,
      stampCount: null,
      stampSerialNo: null,
      stampAmount: null,
    };
    this.stampData.push(data);
  }

  removeStampField(index) {
    this.stampData.splice(index, 1);
  }

  addPropertyDetails() {
    const data = {
      villageName: this.villageName,
      jamabandiSamvat: this.jamabandiSamvat,
      patwarMandal: this.patwarMandal,
      namantranSankhya: this.namantranSankhya,
      upTahsil: this.upTahsil,
      district: this.district,
      tahsil: this.tahsil,
      propertyMeasurementParameters: _.map(
        this.propertyMeasurementParameters,
        (prop) => {
          const p =  {
            accountNo: prop.accountNo,
            khasraNo: prop.khasraNo,
            area: prop.area,
            bhoomiKisma: prop.bhoomiKisma,
            bhoomiHissaType: prop.bhoomiHissaType,
            disha: prop.disha,
            vikrayKshetrafal: prop.vikrayKshetrafal
          };
          if (this.editPropertyIndex) {
            // tslint:disable-next-line:no-string-literal
            p['_id'] = prop._id;
          }
          return p;
        }
      ),
    };
    if (this.editPropertyIndex) {
      // tslint:disable-next-line:no-string-literal
      data['_id'] = this.editPropertyId;
    }
    if (this.editPropertyIndex !== null && this.editPropertyIndex >= 0) {
      this.propertyDetailsData[this.editPropertyIndex] = { ...data };
      this.editPropertyIndex = null;
      this.editPropertyId = null;
    } else {
      this.propertyDetailsData.push({ ...data });
    }
    this.propertyDetailFrom.resetForm();
    const defaultMeasurementParameters = {
      accountNo: null,
      area: null,
      bhoomiKisma: null,
      khasraNo: null,
      bhoomiHissaType: null,
      disha: null,
      vikrayKshetrafal: null
    };
    this.propertyMeasurementParameters = [{ ...defaultMeasurementParameters }];
  }

  editProperty(item, index) {
    this.editPropertyIndex = index;
    this.editPropertyId = item._id;
    this.villageName = item.villageName;
    this.jamabandiSamvat = item.jamabandiSamvat;
    this.patwarMandal = item.patwarMandal;
    this.namantranSankhya = item.namantranSankhya;
    this.upTahsil = item.upTahsil;
    this.tahsil = item.tahsil;
    this.district = item.district;
    this.propertyMeasurementParameters = _.map(
      item.propertyMeasurementParameters,
      (subItem) => {
        return {
          accountNo: subItem.accountNo,
          khasraNo: subItem.khasraNo,
          area: subItem.area,
          bhoomiKisma: subItem.bhoomiKisma,
          bhoomiHissaType: subItem.bhoomiHissaType,
          disha: subItem.disha,
          vikrayKshetrafal: subItem.vikrayKshetrafal
        };
      }
    );
  }

  deleteProperty(index) {
    this.propertyDetailsData.splice(index, 1);
  }

  addPartyDetails() {
    const currentUserType = _.find(this.cs.userTypeList, {
      Id: this.userType,
    });
    const data = {
      userType: this.userType,
      userTypeName: !_.isEmpty(currentUserType) ? currentUserType.Name : '',
      partyName: this.partyName,
      fatherName: this.fatherName,
      relation: this.relation,
      age: this.age,
      cast: this.cast,
      address: this.address,
      aadharNo: this.aadharNo,
      panNo: this.panNo,
      mobileNo: this.mobileNo,
      partyBhoomiHissa: JSON.parse(
        JSON.stringify(this.currentPartyBhoomiHissa)
      ),
    };
    if (this.partyDetailEditIndex !== null && this.partyDetailEditIndex >= 0) {
      this.partyDetailsData[this.partyDetailEditIndex] = { ...data };
      this.partyDetailEditIndex = null;
    } else {
      this.partyDetailsData.push({ ...data });
    }
    this.partyDetailForm.resetForm();
    this.currentPartyBhoomiHissa.forEach((placeItem) => {
      placeItem.propertyMeasurementParameters.forEach((khsraItem) => {
        khsraItem.bhoomiHissa = 0;
        khsraItem.hastantaritHissa = 0;
      });
    });
  }

  editPartyDetail(item, index) {
    this.partyDetailEditIndex = index;
    this.userType = item.userType;
    this.partyName = item.partyName;
    this.fatherName = item.fatherName;
    this.age = item.age;
    this.cast = item.cast;
    this.address = item.address;
    this.aadharNo = item.aadharNo;
    this.panNo = item.panNo;
    this.mobileNo = item.mobileNo;
    this.relation = item.relation;
    this.currentPartyBhoomiHissa = JSON.parse(
      JSON.stringify(item.partyBhoomiHissa)
    );
  }
  deltePartyDetail(index) {
    this.partyDetailsData.splice(index, 1);
  }

  preparePayload(stepId) {
    if (stepId === 1) {
      const filteredCheckAmountData = _.filter(
        this.checkAmountData,
        (check) => {
          if (!_.isEmpty(check) && check.checkNo !== null && check.checkNo) {
            return true;
          } else {
            return false;
          }
        }
      );
      const filteredStampData = _.filter(this.stampData, (stamp) => {
        if (!_.isEmpty(stamp) && (stamp.stampSerialNo || stamp.stampRate)) {
          return true;
        } else {
          return false;
        }
      });
      return {
        documentNo: this.documentNo ? this.documentNo : 0,
        documentTypeId: this.documentTypeId,
        documentLevelId: this.documentLevelId,
        unitOfAreaId: this.unitOfAreaId,
        upPanjiyakKaryalay: this.upPanjiyakKaryalay,
        amountTypeId: this.amountTypeId,
        cashAmount: this.cashAmount,
        checkAmountData: _.map(filteredCheckAmountData, (check) => {
          return {
            paymentType: check.paymentType,
            checkNo: check.checkNo,
            bankName: check.bankName,
            amount: check.amount,
          };
        }),
        stampVendorName: this.stampVendorName,
        stampVendorLicenseNo: this.stampVendorLicenseNo,
        stampVendorAddress: this.stampVendorAddress,
        stampRegisterNo: this.stampRegisterNo,
        stampDate: this.stampDate,
        stampData: _.map(filteredStampData, (stamp) => {
          return {
            stampRate: stamp.stampRate,
            stampCount: stamp.stampCount,
            stampSerialNo: stamp.stampSerialNo,
            stampAmount: stamp.stampAmount,
          };
        }),
      };
    }
    if (stepId === 2) {
      return {
        documentNo: this.documentNo,
        propertyDetailsData: _.map(this.propertyDetailsData, (property) => {
          const p =  {
            villageName: property.villageName,
            jamabandiSamvat: property.jamabandiSamvat,
            patwarMandal: property.patwarMandal,
            namantranSankhya: property.namantranSankhya,
            upTahsil: property.upTahsil,
            tahsil: property.tahsil,
            district: property.district,
            propertyMeasurementParameters: _.map(
              property.propertyMeasurementParameters,
              (item) => {
                const m =  {
                  accountNo: item.accountNo,
                  khasraNo: item.khasraNo,
                  area: item.area,
                  bhoomiKisma: item.bhoomiKisma,
                  bhoomiHissaType: item.bhoomiHissaType,
                  disha: item.disha,
                  vikrayKshetrafal: item.vikrayKshetrafal
                };
                if(item._id){
                  m['_id'] = item._id
                }
                return m;
              }
            ),
          };
          if (property._id) {
            // tslint:disable-next-line:no-string-literal
            p['_id'] = property._id;
          }
          return p;
        }),
      };
    }
    if (stepId === 3) {
      return {
        documentNo: this.documentNo,
        partyDetailsData: _.map(this.partyDetailsData, (party) => {
          const d = [];
          _.forEach(party.partyBhoomiHissa, (placeItem) => {
            _.forEach(placeItem.propertyMeasurementParameters, (khasraItem) => {
              const data = {
                placeId: placeItem._id,
                khasraId: khasraItem._id,
                bhoomiHissa: khasraItem.bhoomiHissa,
                hastantaritHissa: khasraItem.hastantaritHissa,
              };
              d.push({ ...data });
            });
          });
          return {
            userType: party.userType,
            partyName: party.partyName,
            fatherName: party.fatherName,
            age: party.age,
            cast: party.cast,
            address: party.address,
            aadharNo: party.aadharNo,
            panNo: party.panNo,
            mobileNo: party.mobileNo,
            relation: party.relation,
            partyBhoomiHissa:
              party.userType === 1 || party.userType === 2 ? [...d] : [],
          };
        }),
      };
    }
    if (stepId === 4) {
      return {
        documentNo: this.documentNo,
        conditions: {
          distanceFromAabadiArea: this.distanceFromAabadiArea,
          distanceFromKhananArea: this.distanceFromKhananArea,
          distanceFromIndustrialArea: this.distanceFromIndustrialArea,
          nameOfNearbyRoad: this.nameOfNearbyRoad,
          constructionAreaMeasurement: this.constructionAreaMeasurement,
          landTypeId: this.landTypeId,
          mudrankTypeId: this.mudrankTypeId,
          extraaConditions : [...this.extraaConditions],
        },
      };
    }
  }

  assignFormData(data, stepId?) {
    this.documentNo = data.documentNo;
    this.documentLevelId = data.documentLevelId;
    this.documentTypeId = data.documentTypeId;
    this.amountTypeId = data.amountTypeId;
    this.cashAmount = data.cashAmount;
    this.checkAmountData = _.map(data.checkAmountData, (check) => {
      return {
        paymentType: check.paymentType,
        checkNo: check.checkNo,
        bankName: check.bankName,
        amount: check.amount,
      };
    });
    if (_.isEmpty(data.checkAmountData)) {
      const checkDefaultData = {
        paymentType: null,
        checkNo: null,
        bankName: null,
        amount: null,
      };
      this.checkAmountData = [{ ...checkDefaultData }];
    }
    this.stampDate = data.stampDate;
    this.stampRegisterNo = data.stampRegisterNo;
    this.stampVendorName = data.stampVendorName;
    this.stampVendorLicenseNo = data.stampVendorLicenseNo;
    this.stampVendorAddress = data.stampVendorAddress;
    this.unitOfAreaId = data.unitOfAreaId;
    this.upPanjiyakKaryalay = data.upPanjiyakKaryalay;
    this.stampData = _.map(data.stampData, (stamp) => {
      return {
        stampRate: stamp.stampRate,
        stampCount: stamp.stampCount,
        stampSerialNo: stamp.stampSerialNo,
        stampAmount: stamp.stampAmount,
      };
    });
    if (_.isEmpty(data.stampData)) {
      const defaultStampData = {
        id: 0,
        stampRate: null,
        stampCount: null,
        stampSerialNo: null,
        stampAmount: null,
      };
      this.stampData = [{ ...defaultStampData }];
    }
    this.propertyDetailsData = _.map(data.propertyDetailsData, (property) => {
      return {
        _id: property._id,
        propertyMeasurementParameters: _.map(
          property.propertyMeasurementParameters,
          (measureMentData) => {
            return {
              _id: measureMentData._id,
              accountNo: measureMentData.accountNo,
              khasraNo: measureMentData.khasraNo,
              area: measureMentData.area,
              bhoomiKisma: measureMentData.bhoomiKisma,
              bhoomiHissaType: measureMentData.bhoomiHissaType,
              disha: measureMentData.disha,
              vikrayKshetrafal: measureMentData.vikrayKshetrafal
            };
          }
        ),
        villageName: property.villageName,
        jamabandiSamvat: property.jamabandiSamvat,
        patwarMandal: property.patwarMandal,
        namantranSankhya: property.namantranSankhya,
        upTahsil: property.upTahsil,
        tahsil: property.tahsil,
        district: property.district,
      };
    });
    // if (stepId === 2) {
    this.currentPartyBhoomiHissa = JSON.parse(
      JSON.stringify(this.propertyDetailsData)
    );
    // }
    this.partyDetailsData = _.map(data.partyDetailsData, (party) => {
      _.forEach(this.currentPartyBhoomiHissa, (placeItem) => {
        _.forEach(placeItem.propertyMeasurementParameters, (khasraItem) => {
          const d = _.find(party.partyBhoomiHissa, {
            placeId: placeItem._id,
            khasraId: khasraItem._id,
          });
          if (!_.isEmpty(d)) {
            khasraItem.bhoomiHissa = d.bhoomiHissa;
            khasraItem.hastantaritHissa = d.hastantaritHissa;
          }
        });
      });
      const currentUserType = _.find(this.cs.userTypeList, {
        Id: party.userType,
      });
      return {
        userType: party.userType,
        userTypeName: !_.isEmpty(currentUserType) ? currentUserType.Name : '',
        partyName: party.partyName,
        fatherName: party.fatherName,
        age: party.age,
        cast: party.cast,
        address: party.address,
        aadharNo: party.aadharNo,
        panNo: party.panNo,
        mobileNo: party.mobileNo,
        partyBhoomiHissa: JSON.parse(
          JSON.stringify(this.currentPartyBhoomiHissa)
        ),
      };
    });
    if (!_.isEmpty(data.conditions)) {
      this.distanceFromAabadiArea = data.conditions.distanceFromAabadiArea;
      this.distanceFromKhananArea = data.conditions.distanceFromKhananArea;
      this.distanceFromIndustrialArea = data.conditions.distanceFromIndustrialArea;
      this.nameOfNearbyRoad = data.conditions.nameOfNearbyRoad;
      this.constructionAreaMeasurement = data.conditions.constructionAreaMeasurement;
      this.landTypeId = data.conditions.landTypeId;
      this.mudrankTypeId = data.conditions.mudrankTypeId;
      if (!_.isEmpty(data.conditions.extraaConditions)) {
        this.extraaConditions = [...data.conditions.extraaConditions];
      } else {
        this.extraaConditions = [''];
      }
    }
  }

  onStepExit(direction: MovingDirection, stepId): void {
    if (direction === 0) {
      this.submit(stepId);
    }
  }

  submit(stepId?) {
    this.spinner.show();
    const data = this.preparePayload(stepId);
    this.apiService
      .postApi(API_ENDPOINTS.PROPERTY_DOCUMENT, data, true)
      .subscribe(
        (res: any) => {
          if (stepId !== 4) {
            this.assignFormData(res, stepId);
          }
          this.spinner.hide();
          if (stepId === 4) {
            this.closePopUp();
          }
        },
        (err) => {
          console.log(err);
          this.spinner.hide();
        }
      );
  }

  calculateStampAmount(stamp) {
    if (stamp.stampRate && stamp.stampCount) {
      stamp.stampAmount = stamp.stampRate * stamp.stampCount;
    }
  }

  onChangeBhoomiHissa(type) {
    this.currentPartyBhoomiHissa.forEach((placeItem) => {
      placeItem.propertyMeasurementParameters.forEach((khasraItem) => {
        if (type === 'bhoomiHissa') {
          khasraItem.bhoomiHissa = this.bhoomiHissa ? this.bhoomiHissa : 0;
        }
        if (type === 'hastantaritHissa') {
          khasraItem.hastantaritHissa = this.hastantaritHissa
            ? this.hastantaritHissa
            : 0;
        }
        if (type === 'userType') {
          khasraItem.bhoomiHissa = 0;
          khasraItem.hastantaritHissa = 0;
          this.bhoomiHissa = null;
          this.hastantaritHissa = null;
        }
      });
    });
  }

  onChangeBhoomiHissaType(e){

  }
}
