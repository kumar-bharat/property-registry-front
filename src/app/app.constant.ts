export const CONST = {
    AUTH_TOKEN: 'AUTH_TOKEN',
    ERROR_MSG: 'Something went wrong',
    SAVE_SUCCESS_MSG: 'Saved Successfully',
    UPDATE_SUCCESS_MSG: 'Updated Successfully',
    DELETE_SUCCESS_MSG: 'Deleted Successfully',
  };
