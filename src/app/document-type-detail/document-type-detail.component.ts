import { ToasterService } from './../toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from './../api.service';
import { CommonService } from './../common.service';
import { Component, OnInit } from '@angular/core';
import { API_ENDPOINTS } from '../api.endpoint';
import * as _ from 'lodash';
import { Router } from '@angular/router';
@Component({
  selector: 'app-document-type-detail',
  templateUrl: './document-type-detail.component.html',
  styleUrls: ['./document-type-detail.component.scss'],
})
export class DocumentTypeDetailComponent implements OnInit {
  documentNo: any;
  constructor(
    public cs: CommonService,
    private apiService: ApiService,
    private toaster: ToasterService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.getDocumentTypeDetails();
  }

  // preparePayload() {
  //   return {
  //     documentNo: this.cs.documentNo ? this.cs.documentNo : 0,
  //     documentTypeId: this.cs.selectedDocumentTypeId,
  //     unitOfAreaId: this.cs.selectedUnitOfAreaId,
  //     upPanjiyakKaryalay: this.cs.upPanjiyakKaryalay,
  //     documentLevelId: this.cs.documentLevelId,
  //     amountTypeId: this.cs.amountTypeId,
  //     cashAmount: this.cs.cashAmount,
  //     checkAmountData: _.map(this.cs.checkAmountData, (check) => {
  //       return {
  //         checkNo : check.checkNo,
  //         bankName: check.bankName,
  //         amount: check.amount
  //       };
  //     }),
  //     stampVendorName: this.cs.stampVendorName,
  //     stampRegisterNo: this.cs.stampRegisterNo,
  //     stampDate: this.cs.stampDate,
  //     stampData: _.map(this.cs.stampData, (stamp) => {
  //       return {
  //         stampRate: stamp.stampRate,
  //         stampCount: stamp.stampCount,
  //         stampAmount: stamp.stampAmount,
  //         stampSerialNo: stamp.stampSerialNo,
  //       }
  //     })
  //   };
  // }
  submit() {
    // this.spinner.show();
    // this.apiService
    //   .postApi(
    //     API_ENDPOINTS.DOCUMENT_TYPE_DETAIL,
    //     this.preparePayload(),
    //     {},
    //     true
    //   )
    //   .subscribe(
    //     (res: any) => {
    //       this.cs.documentNo = res.documentNo;
    //       this.toaster.showSuccess('TOASTER.SAVE_SUCCESS_MSG');
    //       this.router.navigate(['property-detail']);
    //       this.spinner.hide();
    //     },
    //     (err) => {
    //       console.log(err);
    //       this.toaster.showError('TOASTER.ERROR_MSG');
    //       this.spinner.hide();
    //     }
    //   );
  }

  getDocumentTypeDetails() {
    // this.spinner.show();
    // this.apiService
    //   .getApi(API_ENDPOINTS.DOCUMENT_TYPE_DETAIL, {}, true)
    //   .subscribe(
    //     (res) => {
    //       this.spinner.hide();
    //       if (!_.isEmpty(res)) {
    //         const data = { ...res[0] };
    //         this.cs.selectedDocumentTypeId = data.documentTypeId;
    //         this.cs.selectedUnitOfAreaId = data.unitOfAreaId;
    //         this.cs.upPanjiyakKaryalay = data.upPanjiyakKaryalay;
    //       }
    //     },
    //     (err) => {
    //       this.spinner.hide();
    //       console.log(err);
    //     }
    //   );
  }

  addCheckField() {
    const data = {
      id: 0,
    checkNo: null,
    bankName: null,
    amount: null
    };
    // this.cs.checkAmountData.push(data);
  }

  removeField(index) {
    // this.cs.checkAmountData.splice(index, 1);
  }

  addStampField() {
    const data = {
      id: 0,
    stampRate: null,
    stampCount: null,
    stampSerialNo: null,
    stampAmount: null,
    };
    // this.cs.stampData.push(data);
  }

  removeStampField(index) {
    // this.cs.stampData.splice(index, 1);
  }
}
