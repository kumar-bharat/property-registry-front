import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentTypeDetailComponent } from './document-type-detail.component';

const routes: Routes = [{ path: '', component: DocumentTypeDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentTypeDetailRoutingModule { }
