import { LazyTransModule } from './../lazy-trans.module';
import { SharedModule } from './../shared.module';
import { NgModule } from '@angular/core';
import { DocumentTypeDetailRoutingModule } from './document-type-detail-routing.module';
import { DocumentTypeDetailComponent } from './document-type-detail.component';


@NgModule({
  declarations: [DocumentTypeDetailComponent],
  imports: [
    DocumentTypeDetailRoutingModule,
    SharedModule,
    LazyTransModule
  ]
})
export class DocumentTypeDetailModule { }
