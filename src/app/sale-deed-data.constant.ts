export const Data_Const = {
    vikretaList: [
        {
            fatherName: 'केसुराम',
            vikretaName: 'भरत',
            relation: 'पुत्र',
            age: 25,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 470580822967,
            panNo: 'DWTPP9917B',
            bhoomiHissa: 5,
        },
        {
            fatherName: 'केसुराम',
            vikretaName: 'संदीप',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111,
            panNo: '22222222222',
            bhoomiHissa: 10,
        }
    ],
    kretaList: [
        {
            fatherName: 'राधेश्याम',
            vikretaName: 'नितेश',
            relation: 'पुत्र',
            age: 25,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 470580822967,
            panNo: 'DWTPP9917B',
            bhoomiHissa: 15,
        },
        {
            fatherName: 'राधेश्याम',
            vikretaName: 'जीतेन्द्र',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111,
            panNo: '22222222222',
            bhoomiHissa: 20,
        }
    ],
    witnessList: [
        {
            relativeName: 'बल्लू',
            witnessName: 'गोरु',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111
        },
        {
            relativeName: 'बल्लू',
            witnessName: 'प्रथम',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111
        },
    ],
    propertyParametersList: [
        {
            khataSankhya: 1,
            khasraSankhya: 2,
            kshetrafal: 50,
            bhoomiKism: 'खराब',
            vikrayVivaran: 'सारा बिक गया',
            sinchitType: 'सिंचित',
            jamabandiSamvat: 'पता नहीं',
            patwarMandal: 'पचपहाड़',
            villageName: 'पचपहाड़'
        },
        {
            khataSankhya: 4,
            khasraSankhya: 5,
            kshetrafal: 100,
            bhoomiKism: 'अच्छी',
            vikrayVivaran: 'कुछ नहीं बिका',
            sinchitType: 'सिंचित',
            jamabandiSamvat: 'पता नहीं',
            patwarMandal: 'झालावाड',
            villageName: 'झालावाड'
        }
    ],
    bhoomiAtiriktVivaranList: [
        {
            atiriktVivaran : 'भवानीमंडी की दूरी पचपहाड़ से 100 किमी दूर है!'
        },
        {
            atiriktVivaran : 'पचपहाड़ की दूरी भवानीमंडी से 100 किमी दूर है!'
        },
        {
            atiriktVivaran : 'झालावाड की दूरी भवानीमंडी से 50 किमी दूर है!'
        }
    ],
    checkList: [
        {
            checkAmt: 100,
            checkNo: 200,
            bankName: 'बॉब',
        },
        {
            checkAmt: 100,
            checkNo: 200,
            bankName: 'SBI',
        }
    ],
    amountInNumber: 100000,
    nakadAmount: 50000,
    amountInWords: 'पचास हजार रूपए मात्र',
    stampVendorName: 'महेश',
    stampRegisterNo: 99999,
    stampDate: '22/03/2019',
    stampAmt: 1000,
    licenseNo: 22222222,
    stampVendorAddress: 'सीमेंट गली',
    upPanjiyakKaryalay: 'घर',
    propertyTahsil: 'पचपहाड़',
    propertyUpTahsil: 'पचपहाड़',
    propertyDistrict: 'झालावाड',
    unitOfArea: 'हेक्टेयर'
  };
