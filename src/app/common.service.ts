import { NgxSpinnerService } from 'ngx-spinner';
import { CONST } from './app.constant';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { API_ENDPOINTS } from './api.endpoint';
import * as _ from 'lodash';
import { ToasterService } from './toaster.service';
declare const $: any;

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  userInfo: any = {};
  unitOfAreaList: any[] = [
    {
      Id: 1,
      Name: 'बीघा बिसवा',
    },
    {
      Id: 2,
      Name: 'हेक्टेयर',
    },
    {
      Id: 3,
      Name: 'वर्गफीट',
    },
    {
      Id: 4,
      Name: 'वर्गमीटर',
    },
  ];
  documentTypeList: any[] = [
    {
      Id: 1,
      Name: 'विक्रय पत्र(Sale Deed)',
      path: 'sale-deed'
    },
    {
      Id: 7,
      Name: 'प्लाट रजिस्ट्री',
      path: 'plot-registry'
    },
    {
      Id: 2,
      Name: 'दान पत्र(Gift Deed)',
    },
    {
      Id: 3,
      Name: 'हकत्याग(Release Deed)',
    },
    {
      Id: 7,
      Name: 'गोद पत्र(Adoption)',
    },
    {
      Id: 4,
      Name: 'इच्छापत्र/वसीयत(Will)',
    },
    {
      Id: 5,
      Name: 'Agreement',
    },
    {
      Id: 6,
      Name: 'Power of Attorney',
    },
  ];
  documentShreniList: any[] = [
    {
      Id: 1,
      Name: 'कृषि',
    },
    {
      Id: 2,
      Name: 'आवासीय/व्यावसायिक',
    },
    {
      Id: 3,
      Name: 'औद्योगिक',
    }
  ];
  amountTypeList: any[] = [
    {
      Id: 1,
      Name: 'नगद',
    },
    {
      Id: 2,
      Name: 'नगद एवं अन्य',
    },
    // {
    //   Id: 3,
    //   Name: 'नगद एवं चेक'
    // }
  ];
  userTypeList: any[] = [
    {
      Id: 2,
      Name: 'विक्रेता/दान कर्ता/रिलीज़ कर्ता',
    },
    {
      Id: 1,
      Name: 'क्रेता/दान ग्रहीता/रिलीज़ ग्रहीता',
    },
    {
      Id: 3,
      Name: 'साक्षी',
    },
  ];
  relationList: any[] = [
    {
      Id: 1,
      Name: 'पुत्र',
    },
    {
      Id: 2,
      Name: 'पुत्री',
    },
    {
      Id: 3,
      Name: 'पत्नि',
    },
    {
      Id: 4,
      Name: 'पत्नि स्वर्गीय',
    },
    {
      Id: 5,
      Name: 'दत्तक पुत्र',
    },
    {
      Id: 6,
      Name: 'दत्तक पुत्री',
    },
  ];
  landTypeList: any[] = [
    {
      Id: 1,
      Name: 'सिंचित',
    },
    {
      Id: 2,
      Name: 'असिंचित',
    },
    {
      Id: 3,
      Name: 'सिंचित व असिंचित',
    },
  ];
  mudrankTypeList: any[] = [
    {
      Id: 1,
      Name: 'क्रेता',
    },
    {
      Id: 2,
      Name: 'विक्रेता',
    },
  ];
  hissaTypeList: any[] = [
    {
      Id: 1,
      Name: 'संपूर्ण क्षेत्रफल का विक्रय',
    },
    {
      Id: 2,
      Name: 'आंशिक क्षेत्रफल का विक्रय',
    },
    {
      Id: 3,
      Name: 'हिस्से का विक्रय',
    },
  ];
  paymentTypeList: any[] = [
    {
      Id: 1,
      Name: 'CHECK',
    },
    {
      Id: 2,
      Name: 'NEFT/RTGS',
    },
    {
      Id: 3,
      Name: 'DD',
    },
    {
      Id: 4,
      Name: 'PAY ORDER',
    },
  ];
  langs = ['en', 'hi', 'fr'];
  isLogin: boolean;
  constructor(
    public translateService: TranslateService,
    private router: Router,
    private apiService: ApiService,
    private toaster: ToasterService,
    private spinner: NgxSpinnerService
  ) {
    const localStoarageTokenValue = localStorage.getItem(CONST.AUTH_TOKEN)
    this.isLogin = !this.isEmpty(localStoarageTokenValue)
    if (this.isLogin && _.isEmpty(this.userInfo)) {
      this.getUserInfo();
    }
  }

  public useLanguage(lang: string): void {
    this.translateService.setDefaultLang(lang);
  }
  
  isEmpty(val){
    if(val === null || val === "null" || val === undefined || val === "undefined" || val === '' || val <=0 ){
      return true
    } else{
      return false
    }
  }

  logout() {
    localStorage.removeItem(CONST.AUTH_TOKEN);
    this.isLogin = false;
    this.toaster.showSuccess('Logout Succesfully');
    this.router.navigate(['home']);
  }

  getUserInfo() {
    this.apiService.getApi(API_ENDPOINTS.USER, {}, true).subscribe(
      (res) => {
        if (!_.isEmpty(res)) {
          this.userInfo = { ...res[0] };
        }
      },
      (err) => console.log(err)
    );
  }

  loadModalDynamically(that, containerRef, componentRef, componentToLoad, next, dataToPass?) {
    that[containerRef].clear();
    const factory = that.resolver.resolveComponentFactory(componentToLoad);
    that[componentRef] = that[containerRef].createComponent(factory);
    that[componentRef].instance.openModal(dataToPass);
    that[componentRef].instance.closeModal.subscribe(
      (data) => {
        that[componentRef].destroy();
        next(data);
      });
  }

  openModal(id) {
    $(`#${id}`).modal({ backdrop: 'static', keyboard: false });
    $(`#${id}`).modal('show');
  }

  closeModal(id) {
    $(`#${id}`).modal('hide');
  }
  scrollToTop(){
    window.scrollTo(0, 0)
  }
}
