export const Data_Const = {
    vikretaList: [
        {
            fatherName: 'केसुराम',
            vikretaName: 'भरत',
            relation: 'पुत्र',
            age: 25,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 470580822967,
            panNo: 'DWTPP9917B',
            bhoomiHissa: 5,
        },
        {
            fatherName: 'केसुराम',
            vikretaName: 'संदीप',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111,
            panNo: '22222222222',
            bhoomiHissa: 10,
        }
    ],
    kretaList: [
        {
            fatherName: 'राधेश्याम',
            vikretaName: 'नितेश',
            relation: 'पुत्र',
            age: 25,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 470580822967,
            panNo: 'DWTPP9917B',
            bhoomiHissa: 15,
        },
        {
            fatherName: 'राधेश्याम',
            vikretaName: 'जीतेन्द्र',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111,
            panNo: '22222222222',
            bhoomiHissa: 20,
        }
    ],
    witnessList: [
        {
            relativeName: 'बल्लू',
            witnessName: 'गोरु',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111
        },
        {
            relativeName: 'बल्लू',
            witnessName: 'प्रथम',
            relation: 'पुत्र',
            age: 28,
            cast: 'कुम्हार',
            tahsil: 'पचपहाड़',
            district: 'झालावाड',
            address: 'भवानीमंडी',
            aadharNo: 1111111111
        },
    ],
    plotDetailsList: [
        {
            colonyOrWardNo : "रामनगर",
            plotNo: 1,
            plotArea: 2500,
            constructionArea: 1000,
            constructionDetails: "निर्माण चल रहा है!",
            unit: "हेक्टेयर",
            northBoundary:'1',
            northDirection: 'नोर्थ',
            southBoundary:'2',
            southDirection: 'साउथ',
            eastBoundary:'3',
            eastDirection: 'ईस्ट',
            westBoundary:'4',
            westDirection: 'वेस्ट'
        },
        {
            colonyOrWardNo : "टगर",
            plotNo: 1,
            plotArea: 2500000,
            constructionArea: 1000000,
            constructionDetails: "निर्माण चल रहा है!",
            unit: "हेक्टेयर",
            northBoundary:'5',
            northDirection: 'नोर्थ',
            southBoundary:'6',
            southDirection: 'साउथ',
            eastBoundary:'7',
            eastDirection: 'ईस्ट',
            westBoundary:'8',
            westDirection: 'वेस्ट'
        }
    ],
    bhoomiAtiriktVivaranList: [
        {
            atiriktVivaran : 'भवानीमंडी की दूरी पचपहाड़ से 100 किमी दूर है!'
        },
        {
            atiriktVivaran : 'पचपहाड़ की दूरी भवानीमंडी से 100 किमी दूर है!'
        },
        {
            atiriktVivaran : 'झालावाड की दूरी भवानीमंडी से 50 किमी दूर है!'
        }
    ],
    checkList: [
        {
            checkAmt: 100,
            checkNo: 200,
            bankName: 'बॉब',
        },
        {
            checkAmt: 100,
            checkNo: 200,
            bankName: 'SBI',
        }
    ],
    amountInNumber: 100000,
    nakadAmount: 50000,
    amountInWords: 'पचास हजार रूपए मात्र',
    stampVendorName: 'महेश',
    stampRegisterNo: 99999,
    stampDate: '22/03/2019',
    stampAmt: 1000,
    licenseNo: 22222222,
    stampVendorAddress: 'सीमेंट गली',
    upPanjiyakKaryalay: 'घर',
    propertyDistrict: "मंदसौर",
    propertyTahsil: "नीमच",
    khataSankhya: 4,
    khasraSankhya: 5,
    kshetrafal: 50,
    vikrayVivaran: "बिक गया",
    jamabandiSamvat: "2000-2021",
    patwarMandal: "पचपहाड़",
    villageName: "पचपहाड़",
    propertyUpTahsil: 'पचपहाड़',
    unitOfArea: 'हेक्टेयर'
  };
