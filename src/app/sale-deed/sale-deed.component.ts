import { Component, OnInit } from '@angular/core';
import { CommonService } from './../common.service';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';
import { Data_Const } from '../sale-deed-data.constant';
import Swal from 'sweetalert2';
import { NavigationEnd, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
@Component({
  selector: 'app-sale-deed',
  templateUrl: './sale-deed.component.html',
  styleUrls: ['./sale-deed.component.scss'],
})
export class SaleDeedComponent implements OnInit {
  saleDeedParams: Array<any> = [
    'vikretaList',
    'kretaList',
    'checkList',
    'amountInNumber',
    'nakadAmount',
    'amountInWords',
    'stampVendorName',
    'stampRegisterNo',
    'stampDate',
    'stampAmt',
    'licenseNo',
    'stampVendorAddress',
    'upPanjiyakKaryalay',
    'propertyTahsil',
    'propertyUpTahsil',
    'propertyDistrict',
    'unitOfArea',
    'propertyParametersList',
    'witnessList',
    'bhoomiAtiriktVivaranList',
  ];
  vikretaObj = {
    fatherName: '',
    vikretaName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
    panNo: '',
    bhoomiHissa: '',
  };
  vikretaList: Array<any> = [{ ...this.vikretaObj }];
  kretaObj = {
    fatherName: '',
    vikretaName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
    panNo: '',
    bhoomiHissa: '',
  };
  kretaList: Array<any> = [{ ...this.kretaObj }];
  amountInNumber: number = null;
  nakadAmount: number = null;
  amountInWords: string = '';
  checkListObj = {
    checkAmt: null,
    checkNo: null,
    bankName: '',
  };
  checkList: Array<any> = [{ ...this.checkListObj }];
  stampVendorName: string = '';
  stampRegisterNo: number = null;
  stampDate: any = null;
  stampAmt: number = null;
  licenseNo: number = null;
  stampVendorAddress: string = '';
  upPanjiyakKaryalay: string = '';
  propertyTahsil: string = '';
  propertyUpTahsil: string = '';
  propertyDistrict: string = '';
  unitOfArea: string = null;
  propertyParameterObj = {
    khataSankhya: null,
    khasraSankhya: null,
    kshetrafal: null,
    bhoomiKism: '',
    vikrayVivaran: '',
    sinchitType: null,
    jamabandiSamvat: '',
    patwarMandal: '',
    villageName: '',
  };
  propertyParametersList: Array<any> = [{ ...this.propertyParameterObj }];
  witnessObj = {
    relativeName: '',
    witnessName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
  };
  witnessList = [{ ...this.witnessObj }, { ...this.witnessObj }];
  bhoomiAtiriktVivaranObj: any = {
    atiriktVivaran: '',
  };
  bhoomiAtiriktVivaranList = [{ ...this.bhoomiAtiriktVivaranObj }];
  constructor(public cs: CommonService, private router: Router, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.cs.scrollToTop();
  }

  addDataFromJson() {
    this.saleDeedParams.forEach((element) => {
      this[element] = Data_Const[element];
    });
  }

  showConfirmDialog(cb) {
    Swal.fire({
      title: 'Do you want to confirm Delete?',
      showDenyButton: true,
      // showCancelButton: true,
      confirmButtonText: `Ok`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        cb();
      }
    });
  }

  addVikreta() {
    this.vikretaList.push({ ...this.vikretaObj });
  }
  deleteVikreta(index) {
    this.showConfirmDialog(() => {
      this.vikretaList.splice(index, 1);
    });
  }
  addKreta() {
    this.kretaList.push({ ...this.kretaObj });
  }
  deleteKreta(index) {
    this.showConfirmDialog(() => {
      this.kretaList.splice(index, 1);
    });
  }
  addCheck() {
    this.checkList.push({ ...this.checkListObj });
  }
  deleteCheck(index) {
    this.showConfirmDialog(() => {
      this.checkList.splice(index, 1);
    });
  }
  addPropertyParameters() {
    this.propertyParametersList.push({ ...this.propertyParameterObj });
  }
  deletePropertyParameters(index) {
    this.showConfirmDialog(() => {
      this.propertyParametersList.splice(index, 1);
    });
  }
  addAtiriktVivran() {
    this.bhoomiAtiriktVivaranList.push({ ...this.bhoomiAtiriktVivaranObj });
  }
  deleteAtiriktVivran(index) {
    this.showConfirmDialog(() => {
      this.bhoomiAtiriktVivaranList.splice(index, 1);
    });
  }

  submitForm() {
    const data = {};
    data['vikreta'] = _.map(this.vikretaList, (item) => {
      return {
        name: item.vikretaName,
        relation: item.relation,
        relative_name: item.fatherName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo,
        pan: item.panNo,
        hissa: item.bhoomiHissa,
      };
    });

    data['kreta'] = _.map(this.kretaList, (item) => {
      return {
        name: item.vikretaName,
        relation: item.relation,
        relative_name: item.fatherName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo,
        pan: item.panNo,
      };
    });

    data['witness'] = _.map(this.witnessList, (item) => {
      return {
        name: item.witnessName,
        relation: item.relation,
        relative_name: item.relativeName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo,
      };
    });

    data['bhoomi'] = _.map(this.propertyParametersList, (item) => {
      return {
        village: item.villageName ? item.villageName : "",
        mandal: item.patwarMandal ? item.patwarMandal  : "",
        khata: item.khataSankhya ? item.khataSankhya  : "",
        khasra: item.khasraSankhya ? item.khasraSankhya  : "",
        area: item.kshetrafal ? item.kshetrafal : "",
        kism: item.bhoomiKism ? item.bhoomiKism : "",
        vikray_vivaran: item.vikrayVivaran ? item.vikrayVivaran : "",
        sinchitType: item.sinchitType ? item.sinchitType : "",
        jamabandi: item.jamabandiSamvat ? item.jamabandiSamvat : "",
        unitOfArea: this.unitOfArea ? this.unitOfArea : "",
      };
    });
    console.log("bhoomi data", data['bhoomi']);
    data['atirikt_vivaran'] = _.map(this.bhoomiAtiriktVivaranList, (item) => {
      return {
        vivaran: item.atiriktVivaran,
      };
    });
    this.generateCombineZip(data);
  }

  generateCombineZip = async (Data) => {
    this.spinner.show()
    const outputZip = new PizZip();
    const buffer1 = await this.generateAsync(Data);
    outputZip.file('agriculture-registry.docx', buffer1);
    const buffer2 = await this.generateCheckListAsync(Data);
    outputZip.file('checklist-agri.docx', buffer2);
    this.spinner.hide()
    const outputBlob = outputZip.generate({
      type: 'blob',
      compression: 'DEFLATE',
      mimeType:
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    });
    saveAs(outputBlob, 'agriculture.zip');
  };

  generateAsync = (Data) => {
    return new Promise((resolve, reject) => {
      loadFile('../../assets/files/agriculture-registry.docx', async (error, content) => {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater(zip);
        await doc.setData({
          vikreta: [...Data.vikreta],
          kreta: [...Data.kreta],
          witness: [...Data.witness],
          bh: [...Data.bhoomi],
          av: [...Data.atirikt_vivaran],
          vikray_amt_in_number: this.amountInNumber,
          vikray_amt_in_words: this.amountInWords,
          bhoomi_vivran_district: this.propertyDistrict,
          bhoomi_vivran_tahsil: this.propertyTahsil,
          first_mandal: this.propertyParametersList[0].patwarMandal,
          first_village: this.propertyParametersList[0].villageName,
          first_jamabandi: this.propertyParametersList[0].jamabandiSamvat,
          stamp_amt: this.stampAmt,
          unit: this.unitOfArea,
        });
        try {
          await doc.render();
        } catch (error) {
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function (
                error,
                key
              ) {
                error[key] = value[key];
                return error;
              },
              {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));
          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function (error) {
                return error.properties.explanation;
              })
              .join('\n');
            console.log('errorMessages', errorMessages);
          }
          throw error;
        }
        const buffer1 = await doc.getZip().generate({
          type: 'arraybuffer',
          compression: 'DEFLATE',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        resolve(buffer1);
      });
    });
  };

  generateCheckListAsync = (Data) => {
    return new Promise((resolve, reject) => {
      loadFile(
        '../../assets/files/checklist-agri.docx',
        async (error, content) => {
          if (error) {
            throw error;
          }
          var zip = new PizZip(content);
          var doc = new Docxtemplater(zip);
          await doc.setData({
            vik: [...Data.vikreta],
            kreta: [...Data.kreta],
            witness: [...Data.witness],
            bh: [...Data.bhoomi],
            av: [...Data.atirikt_vivaran],
            vikray_amt_in_number: this.amountInNumber,
            vikray_amt_in_words: this.amountInWords,
            stamp_amt: this.stampAmt,
            vendorName: this.stampVendorName,
            vendorSrNo: this.stampRegisterNo,
            stamp_date: this.stampDate,
            district: this.propertyDistrict,
            tahsil: this.propertyTahsil,
            upPanjiyakKaryalay: this.upPanjiyakKaryalay,
            first_mandal: this.propertyParametersList[0].patwarMandal,
            first_village: this.propertyParametersList[0].villageName,
            first_jamabandi: this.propertyParametersList[0].jamabandiSamvat,
          });
          try {
            await doc.render();
          } catch (error) {
            function replaceErrors(key, value) {
              if (value instanceof Error) {
                return Object.getOwnPropertyNames(value).reduce(function (
                  error,
                  key
                ) {
                  error[key] = value[key];
                  return error;
                },
                {});
              }
              return value;
            }
            console.log(JSON.stringify({ error: error }, replaceErrors));
            if (error.properties && error.properties.errors instanceof Array) {
              const errorMessages = error.properties.errors
                .map(function (error) {
                  return error.properties.explanation;
                })
                .join('\n');
              console.log('errorMessages', errorMessages);
            }
            throw error;
          }
          const buffer2 = await doc.getZip().generate({
            type: 'arraybuffer',
            compression: 'DEFLATE',
            mimeType:
              'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
          });
          resolve(buffer2);
        }
      );
    });
  };

  generate = (Data) => {
    loadFile('../../assets/files/sale-deed.docx', async (error, content) => {
      if (error) {
        throw error;
      }
      var zip = new PizZip(content);
      var doc = new Docxtemplater(zip);
      doc.setData({
        vikreta: [...Data.vikreta],
        kreta: [...Data.kreta],
        witness: [...Data.witness],
        bh: [...Data.bhoomi],
        av: [...Data.atirikt_vivaran],
        vikray_amt_in_number: this.amountInNumber,
        vikray_amt_in_words: this.amountInWords,
        bhoomi_vivran_district: this.propertyDistrict,
        bhoomi_vivran_tahsil: this.propertyTahsil,
        first_mandal: this.propertyParametersList[0].patwarMandal,
        first_village: this.propertyParametersList[0].villageName,
        first_jamabandi: this.propertyParametersList[0].jamabandiSamvat,
        stamp_amt: this.stampAmt,
        unit: this.unitOfArea,
      });
      try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render();
      } catch (error) {
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
        function replaceErrors(key, value) {
          if (value instanceof Error) {
            return Object.getOwnPropertyNames(value).reduce(function (
              error,
              key
            ) {
              error[key] = value[key];
              return error;
            },
            {});
          }
          return value;
        }
        console.log(JSON.stringify({ error: error }, replaceErrors));

        if (error.properties && error.properties.errors instanceof Array) {
          const errorMessages = error.properties.errors
            .map(function (error) {
              return error.properties.explanation;
            })
            .join('\n');
          console.log('errorMessages', errorMessages);
          // errorMessages is a humanly readable message looking like this :
          // 'The tag beginning with "foobar" is unopened'
        }
        throw error;
      }
      // var out = await doc.getZip().generate({
      //   type: 'blob',
      //   mimeType:
      //     'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      // }); //Output the document using Data-URI
      // saveAs(out, 'sale-deed.docx');
      const buffer1 = doc.getZip().generate({
        type: 'arraybuffer',
        compression: 'DEFLATE',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      });
      return buffer1;
    });
  };

  generateCheckList = (Data) => {
    loadFile(
      '../../assets/files/sale-deed-check-list.docx',
      async (error, content) => {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater(zip);
        doc.setData({
          vik: [...Data.vikreta],
          kreta: [...Data.kreta],
          witness: [...Data.witness],
          bh: [...Data.bhoomi],
          av: [...Data.atirikt_vivaran],
          vikray_amt_in_number: this.amountInNumber,
          vikray_amt_in_words: this.amountInWords,
          stamp_amt: this.stampAmt,
          vendorName: this.stampVendorName,
          vendorSrNo: this.stampRegisterNo,
          stamp_date: this.stampDate,
          district: this.propertyDistrict,
          tahsil: this.propertyTahsil,
          upPanjiyakKaryalay: this.upPanjiyakKaryalay,
          first_mandal: this.propertyParametersList[0].patwarMandal,
          first_village: this.propertyParametersList[0].villageName,
          first_jamabandi: this.propertyParametersList[0].jamabandiSamvat,
        });
        try {
          // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
          doc.render();
        } catch (error) {
          // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function (
                error,
                key
              ) {
                error[key] = value[key];
                return error;
              },
              {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));

          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function (error) {
                return error.properties.explanation;
              })
              .join('\n');
            console.log('errorMessages', errorMessages);
            // errorMessages is a humanly readable message looking like this :
            // 'The tag beginning with "foobar" is unopened'
          }
          throw error;
        }
        // var out = await doc.getZip().generate({
        //   type: 'blob',
        //   mimeType:
        //     'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        // }); //Output the document using Data-URI
        // saveAs(out, 'check-list.docx');
        const buffer2 = doc.getZip().generate({
          type: 'arraybuffer',
          compression: 'DEFLATE',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        return buffer2;
      }
    );
  };
}
