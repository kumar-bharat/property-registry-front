import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleDeedRoutingModule } from './sale-deed-routing.module';
import { SaleDeedComponent } from './sale-deed.component';
import { SharedModule } from './../shared.module';

@NgModule({
  declarations: [SaleDeedComponent],
  imports: [CommonModule, SaleDeedRoutingModule, SharedModule],
})
export class SaleDeedModule {}
