import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdGuard } from '../auth-gaurd.guard';
import { SaleDeedComponent } from './sale-deed.component';

const routes: Routes = [{ path: '', canActivate: [AuthGaurdGuard], component: SaleDeedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaleDeedRoutingModule { }
