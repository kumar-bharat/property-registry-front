export const API_ENDPOINTS = {
    LOGIN: 'login',
    SIGN_UP: 'signup',
    USER: 'user',
    ALL_USERS: 'alluser',
    PARTY_DETAIL: 'partydetail',
    DOCUMENT_TYPE_DETAIL: 'documentTypeDetail',
    PROPERTY_DETAIL: 'propertyDetail',
    PROPERTY_DOCUMENT: 'propertyDocument',
  };
