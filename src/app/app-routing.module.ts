import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdGuard } from './auth-gaurd.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'home/:type',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./app-start/dashboard.module').then((m) => m.DashBoardModule),
  },
  {
    path: 'signin-signup',
    loadChildren: () =>
      import('./signin-signup/signin-signup.module').then(
        (m) => m.SigninSignupModule
      ),
  },
  { path: 'sale-deed', canLoad: [AuthGaurdGuard], loadChildren: () => import('./sale-deed/sale-deed.module').then(m => m.SaleDeedModule) },
  { path: 'plot-registry', canLoad: [AuthGaurdGuard], loadChildren: () => import('./plot-registry/plot-registry.module').then(m => m.PlotRegistryModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
