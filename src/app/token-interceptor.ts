import { CommonService } from './common.service';
import { ToasterService } from './toaster.service';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpResponse, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private injector: Injector,
              private router: Router) { }
  intercept(req, next) {
    return next.handle(req).pipe(
        map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
            }
            return event;
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 401 || error.status === 400) {
            // this.toaster.showError('', error.error.message);
            // localStorage.clear();
            // this.router.navigate(['signin-signup'], {queryParams : { type : 'signin'}});
          }
          return throwError(error);
      }));
  }
}
