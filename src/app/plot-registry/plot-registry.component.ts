import { Component, OnInit } from '@angular/core';
import { CommonService } from './../common.service';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';
import { Data_Const } from '../plot-registry-data.constant';
import Swal from 'sweetalert2'
import { NavigationEnd, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}

@Component({
  selector: 'app-plot-registry',
  templateUrl: './plot-registry.component.html',
  styleUrls: ['./plot-registry.component.scss']
})
export class PlotRegistryComponent implements OnInit {
  plotDeedParams: Array<any> = [
    'vikretaList',
    'kretaList',
    'checkList',
    'amountInNumber',
    'nakadAmount',
    'amountInWords',
    'stampVendorName',
    'stampRegisterNo',
    'stampDate',
    'stampAmt',
    'licenseNo',
    'stampVendorAddress',
    'upPanjiyakKaryalay',
    'villageName',
    'patwarMandal',
    'jamabandiSamvat',
    'propertyTahsil',
    'propertyUpTahsil',
    'propertyDistrict',
    'plotDetailsList',
    'witnessList',
    'bhoomiAtiriktVivaranList',
    'khataSankhya',
    'khasraSankhya',
    'kshetrafal',
    'vikrayVivaran'
  ]
  vikretaObj = {
    fatherName: '',
    vikretaName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
    panNo: '',
    bhoomiHissa: '',
  };
  vikretaList: Array<any> = [{ ...this.vikretaObj }];
  kretaObj = {
    fatherName: '',
    vikretaName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
    panNo: '',
    bhoomiHissa: '',
  };
  kretaList: Array<any> = [{ ...this.kretaObj }];
  amountInNumber: number = null;
  nakadAmount: number = null;
  amountInWords: string = '';
  checkListObj = {
    checkAmt: null,
    checkNo: null,
    bankName: '',
  };
  checkList: Array<any> = [{ ...this.checkListObj }];
  stampVendorName: string = '';
  stampRegisterNo: number = null;
  stampDate: any = null;
  stampAmt: number = null;
  licenseNo: number = null;
  stampVendorAddress: string = '';
  upPanjiyakKaryalay: string = '';
  propertyTahsil: string = '';
  propertyUpTahsil: string = '';
  propertyDistrict: string = '';
  khataSankhya: number = null;
  khasraSankhya: number = null;
  kshetrafal: number = null;
  vikrayVivaran: string = ''
  jamabandiSamvat: string = ''
  patwarMandal: string = ''
  villageName: string = ''
  plotDetailsObj = {
    colonyOrWardNo : null,
    plotNo: null,
    plotArea: null,
    constructionArea: null,
    constructionDetails:null,
    unit: null,
    northBoundary:'',
    northDirection: '',
    southBoundary:'',
    southDirection: '',
    eastBoundary:'',
    eastDirection: '',
    westBoundary:'',
    westDirection: ''
  }
  plotDetailsList: Array<any> = [{ ...this.plotDetailsObj }];
  purvaVartiVivran: string = '';
  bookNo: string = '';
  jildNo: string = '';
  pageNo: string = '';
  panjiyanNo: string = '';
  panjiyanDate: string = '';
  pattaNo: string = '';
  pattaInitiateDate: any = null;
  witnessObj = {
    relativeName: '',
    witnessName: '',
    relation: null,
    age: null,
    cast: '',
    tahsil: '',
    district: '',
    address: '',
    aadharNo: null,
  };
  witnessList = [{ ...this.witnessObj }, { ...this.witnessObj }];
  bhoomiAtiriktVivaranObj: any = {
    atiriktVivaran: ''
  };
  bhoomiAtiriktVivaranList = [{...this.bhoomiAtiriktVivaranObj}]
  constructor(public cs: CommonService, private router: Router, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.cs.scrollToTop()
  }

  addDataFromJson(){
    this.plotDeedParams.forEach((element) => {
      this[element] = Data_Const[element]
    });
  }

  showConfirmDialog(cb){
    Swal.fire({
      title: 'Do you want to confirm Delete?',
      showDenyButton: true,
      // showCancelButton: true,
      confirmButtonText: `Ok`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        cb()
      }
    })
  }

  addVikreta() {
    this.vikretaList.push({...this.vikretaObj});
  }
  deleteVikreta(index) {
    this.showConfirmDialog(()=> {
      this.vikretaList.splice(index, 1);
    })
  }
  addKreta() {
    this.kretaList.push({...this.kretaObj});
  }
  deleteKreta(index) {
    this.showConfirmDialog(()=> {
      this.kretaList.splice(index, 1);
    })
  }
  addCheck() {
    this.checkList.push({...this.checkListObj});
  }
  deleteCheck(index) {
    this.showConfirmDialog(()=> {
      this.checkList.splice(index, 1);
    })
  }
  addPlotDetails() {
    this.plotDetailsList.push({...this.plotDetailsObj});
  }
  deletePlotDetails(index) {
    this.showConfirmDialog(()=> {
      this.plotDetailsList.splice(index, 1);
    })
  }
  addAtiriktVivran(){
    this.bhoomiAtiriktVivaranList.push({...this.bhoomiAtiriktVivaranObj});
  }
  deleteAtiriktVivran(index) {
    this.showConfirmDialog(()=> {
      this.bhoomiAtiriktVivaranList.splice(index, 1);
    })
  }

  submitForm() {
    const data = {};
    data['vikreta'] = _.map(this.vikretaList, (item) => {
      return {
        name: item.vikretaName,
        relation: item.relation,
        relative_name: item.fatherName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo,
        pan: item.panNo,
        hissa: item.bhoomiHissa
      };
    });

    data['kreta'] = _.map(this.kretaList, (item) => {
      return {
        name: item.vikretaName,
        relation: item.relation,
        relative_name: item.fatherName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo,
        pan: item.panNo,
      };
    });

    data['witness'] = _.map(this.witnessList, (item) => {
      return {
        name: item.witnessName,
        relation: item.relation,
        relative_name: item.relativeName,
        age: item.age,
        caste: item.cast,
        address: item.address,
        tahsil: item.tahsil,
        district: item.district,
        aadhar: item.aadharNo
      };
    });

    data['plot'] = _.map(this.plotDetailsList, (item) => {
      return {
        colonyOrWardNo : item.colonyOrWardNo,
        plotNo: item.plotNo,
        plotArea: item.plotArea,
        constructionArea: item.constructionArea,
        constructionDetails:item.constructionDetails,
        unit: item.unit,
        northBoundary: item.northBoundary,
        northDirection: item.northDirection,
        southBoundary: item.southBoundary,
        southDirection: item.southDirection,
        eastBoundary: item.eastBoundary,
        eastDirection: item.eastDirection,
        westBoundary: item.westBoundary,
        westDirection: item.westDirection,
      };
    });
    data['atirikt_vivaran'] = _.map(this.bhoomiAtiriktVivaranList, (item) => {
      return {
        vivaran: item.atiriktVivaran
      };
    });
    // this.generateCheckList(data);
    this.generateCombineZip(data);
  }

  generate = (Data) => {
    loadFile('../../assets/files/sale-deed.docx', async (error, content) => {
      if (error) {
        throw error;
      }
      var zip = new PizZip(content);
      var doc = new Docxtemplater(zip);
      doc.setData({
        vikreta: [...Data.vikreta],
        kreta: [...Data.kreta],
        witness: [...Data.witness],
        bh: [...Data.bhoomi],
        av: [...Data.atirikt_vivaran],
        vikray_amt_in_number: this.amountInNumber,
        vikray_amt_in_words: this.amountInWords,
        bhoomi_vivran_district: this.propertyDistrict,
        bhoomi_vivran_tahsil: this.propertyTahsil,
        // first_mandal: this.plotDetailsList[0].patwarMandal,
        // first_village: this.plotDetailsList[0].villageName,
        // first_jamabandi: this.plotDetailsList[0].jamabandiSamvat,
        // stamp_amt: this.stampAmt,
        // unit: this.unitOfArea
      });
      try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render();
      } catch (error) {
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
        function replaceErrors(key, value) {
          if (value instanceof Error) {
            return Object.getOwnPropertyNames(value).reduce(function (
              error,
              key
            ) {
              error[key] = value[key];
              return error;
            },
            {});
          }
          return value;
        }
        console.log(JSON.stringify({ error: error }, replaceErrors));

        if (error.properties && error.properties.errors instanceof Array) {
          const errorMessages = error.properties.errors
            .map(function (error) {
              return error.properties.explanation;
            })
            .join('\n');
          console.log('errorMessages', errorMessages);
          // errorMessages is a humanly readable message looking like this :
          // 'The tag beginning with "foobar" is unopened'
        }
        throw error;
      }
      var out = await doc.getZip().generate({
        type: 'blob',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      }); //Output the document using Data-URI
      saveAs(out, 'sale-deed.docx');
    });
  };

  generateCheckList = (Data) => {
    loadFile('../../assets/files/plot-checklist-final.docx', async (error, content) => {
      if (error) {
        throw error;
      }
      var zip = new PizZip(content);
      var doc = new Docxtemplater(zip);
      doc.setData({
        vik: [...Data.vikreta],
        kreta: [...Data.kreta],
        witness: [...Data.witness],
        plot: [...Data.plot],
        av: [...Data.atirikt_vivaran],
        vikray_amt_in_number: this.amountInNumber,
        vikray_amt_in_words: this.amountInWords,
        stamp_amt: this.stampAmt,
        vendorName: this.stampVendorName,
        vendorSrNo: this.stampRegisterNo,
        stamp_date: this.stampDate,
        district: this.propertyDistrict,
        tahsil: this.propertyTahsil,
        upPanjiyakKaryalay: this.upPanjiyakKaryalay,
        khata: this.khataSankhya,
        khasra: this.khasraSankhya,
        area: this.kshetrafal,
        unitOfArea: this.plotDetailsList[0].unit,
        vikray_vivaran: this.vikrayVivaran,
        jamabandiSamvat: this.jamabandiSamvat,
        patwarMandal: this.patwarMandal,
        villageName: this.villageName,
      });
      try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render();
      } catch (error) {
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
        function replaceErrors(key, value) {
          if (value instanceof Error) {
            return Object.getOwnPropertyNames(value).reduce(function (
              error,
              key
            ) {
              error[key] = value[key];
              return error;
            },
            {});
          }
          return value;
        }
        console.log(JSON.stringify({ error: error }, replaceErrors));

        if (error.properties && error.properties.errors instanceof Array) {
          const errorMessages = error.properties.errors
            .map(function (error) {
              return error.properties.explanation;
            })
            .join('\n');
          console.log('errorMessages', errorMessages);
          // errorMessages is a humanly readable message looking like this :
          // 'The tag beginning with "foobar" is unopened'
        }
        throw error;
      }
      var out = await doc.getZip().generate({
        type: 'blob',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      }); //Output the document using Data-URI
      saveAs(out, 'plot-registry.doc');
    });
  };

  generateRegistryAsync(Data){
    return new Promise((resolve, reject) => {
      loadFile('../../assets/files/plot-registry-final.docx', async (error, content) => {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater(zip);
        await doc.setData({
          vik: [...Data.vikreta],
          kreta: [...Data.kreta],
          witness: [...Data.witness],
          plot: [...Data.plot],
          av: [...Data.atirikt_vivaran],
          vikray_amt_in_number: this.amountInNumber,
          vikray_amt_in_words: this.amountInWords,
          stamp_amt: this.stampAmt,
          vendorName: this.stampVendorName,
          vendorSrNo: this.stampRegisterNo,
          stamp_date: this.stampDate,
          district: this.propertyDistrict,
          tahsil: this.propertyTahsil,
          upPanjiyakKaryalay: this.upPanjiyakKaryalay,
          khata: this.khataSankhya,
          khasra: this.khasraSankhya,
          area: this.kshetrafal,
          unitOfArea: this.plotDetailsList[0].unit,
          vikray_vivaran: this.vikrayVivaran,
          jamabandiSamvat: this.jamabandiSamvat,
          patwarMandal: this.patwarMandal,
          villageName: this.villageName,
        });
        try {
          // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
          await doc.render();
        } catch (error) {
          // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function (
                error,
                key
              ) {
                error[key] = value[key];
                return error;
              },
              {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));
  
          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function (error) {
                return error.properties.explanation;
              })
              .join('\n');
            console.log('errorMessages', errorMessages);
            // errorMessages is a humanly readable message looking like this :
            // 'The tag beginning with "foobar" is unopened'
          }
          throw error;
        }
        // var out = await doc.getZip().generate({
        //   type: 'blob',
        //   mimeType:
        //     'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        // }); //Output the document using Data-URI
        // saveAs(out, 'plot-registry.doc');
        const buffer2 = await doc.getZip().generate({
          type: 'arraybuffer',
          compression: 'DEFLATE',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        resolve(buffer2);
      });
    });
  }

  generateCheckListAsync(Data){
    return new Promise((resolve, reject) => {
      loadFile('../../assets/files/plot-checklist-final.docx', async (error, content) => {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater(zip);
        await doc.setData({
          vik: [...Data.vikreta],
          kreta: [...Data.kreta],
          witness: [...Data.witness],
          plot: [...Data.plot],
          av: [...Data.atirikt_vivaran],
          vikray_amt_in_number: this.amountInNumber,
          vikray_amt_in_words: this.amountInWords,
          stamp_amt: this.stampAmt,
          vendorName: this.stampVendorName,
          vendorSrNo: this.stampRegisterNo,
          stamp_date: this.stampDate,
          district: this.propertyDistrict,
          tahsil: this.propertyTahsil,
          upPanjiyakKaryalay: this.upPanjiyakKaryalay,
          khata: this.khataSankhya,
          khasra: this.khasraSankhya,
          area: this.kshetrafal,
          unitOfArea: this.plotDetailsList[0].unit,
          vikray_vivaran: this.vikrayVivaran,
          jamabandiSamvat: this.jamabandiSamvat,
          patwarMandal: this.patwarMandal,
          villageName: this.villageName,
        });
        try {
          // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
          await doc.render();
        } catch (error) {
          // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
          function replaceErrors(key, value) {
            if (value instanceof Error) {
              return Object.getOwnPropertyNames(value).reduce(function (
                error,
                key
              ) {
                error[key] = value[key];
                return error;
              },
              {});
            }
            return value;
          }
          console.log(JSON.stringify({ error: error }, replaceErrors));
  
          if (error.properties && error.properties.errors instanceof Array) {
            const errorMessages = error.properties.errors
              .map(function (error) {
                return error.properties.explanation;
              })
              .join('\n');
            console.log('errorMessages', errorMessages);
            // errorMessages is a humanly readable message looking like this :
            // 'The tag beginning with "foobar" is unopened'
          }
          throw error;
        }
        // var out = await doc.getZip().generate({
        //   type: 'blob',
        //   mimeType:
        //     'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        // }); //Output the document using Data-URI
        // saveAs(out, 'plot-registry.doc');
        const buffer1 = await doc.getZip().generate({
          type: 'arraybuffer',
          compression: 'DEFLATE',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        resolve(buffer1);
      });
    });
  }

  generateCombineZip = async (Data) => {
    this.spinner.show()
    const outputZip = new PizZip();
    const buffer1 = await this.generateRegistryAsync(Data);
    outputZip.file('registry.docx', buffer1);
    const buffer2 = await this.generateCheckListAsync(Data);
    outputZip.file('check-list.docx', buffer2);
    this.spinner.hide()
    const outputBlob = outputZip.generate({
      type: 'blob',
      compression: 'DEFLATE',
      mimeType:
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    });
    saveAs(outputBlob, 'plot-registry.zip');
  };
}
