import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotRegistryComponent } from './plot-registry.component';

describe('PlotRegistryComponent', () => {
  let component: PlotRegistryComponent;
  let fixture: ComponentFixture<PlotRegistryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlotRegistryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlotRegistryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
