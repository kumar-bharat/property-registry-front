import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdGuard } from '../auth-gaurd.guard';
import { PlotRegistryComponent } from './plot-registry.component';

const routes: Routes = [{ path: '', canActivate: [AuthGaurdGuard], component: PlotRegistryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlotRegistryRoutingModule { }
