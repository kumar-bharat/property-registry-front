import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared.module';
import { PlotRegistryRoutingModule } from './plot-registry-routing.module';
import { PlotRegistryComponent } from './plot-registry.component';


@NgModule({
  declarations: [PlotRegistryComponent],
  imports: [
  CommonModule,
    PlotRegistryRoutingModule,
    SharedModule
  ]
})
export class PlotRegistryModule { }
