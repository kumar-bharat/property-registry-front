import { LazyTransModule } from './../lazy-trans.module';
import { SharedModule } from './../shared.module';
import { NgModule } from '@angular/core';
import { PartyDetailRoutingModule } from './party-detail-routing.module';
import { PartyDetailComponent } from './party-detail.component';


@NgModule({
  declarations: [PartyDetailComponent],
  imports: [
    PartyDetailRoutingModule,
    SharedModule,
    LazyTransModule
  ]
})
export class PartyDetailModule { }
