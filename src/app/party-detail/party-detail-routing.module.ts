import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartyDetailComponent } from './party-detail.component';

const routes: Routes = [{ path: '', component: PartyDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartyDetailRoutingModule { }
