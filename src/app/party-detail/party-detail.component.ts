import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from './../toaster.service';
import { ApiService } from './../api.service';
import { CommonService } from './../common.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { API_ENDPOINTS } from '../api.endpoint';
import * as _ from 'lodash';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-party-detail',
  templateUrl: './party-detail.component.html',
  styleUrls: ['./party-detail.component.scss'],
})
export class PartyDetailComponent implements OnInit {
  @ViewChild('partyDetailForm') partyDetailForm: NgForm;
  selectedUserTypeId: number;
  mobileNo: number;
  panNo: any;
  adharNo: number;
  userName: string;
  address: string;
  cast: string;
  age: string;
  fatherName: string;
  partyDetailList: any[] = [];
  editId: any;
  constructor(
    public cs: CommonService,
    private apiService: ApiService,
    private toaster: ToasterService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.getpartyDetailList();
  }

  preparePayload() {
    return {
      // documentNo: this.cs.documentNo,
      userType: this.selectedUserTypeId,
      name: this.userName,
      fatherName: this.fatherName,
      age: this.age,
      cast: this.cast,
      address: this.address,
      aadharNo: this.adharNo,
      panNo: this.panNo,
      mobileNo: this.mobileNo,
    };
  }
  submit() {
    this.spinner.show();
    const query = this.editId ? { id: this.editId } : {};
    this.apiService
      .postApi(API_ENDPOINTS.PARTY_DETAIL, this.preparePayload(), true, query)
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.partyDetailForm.resetForm();
          if (this.editId) {
            this.editId = null;
            this.toaster.showSuccess('TOASTER.SAVE_SUCCESS_MSG');
          } else {
            this.toaster.showSuccess('TOASTER.UPDATE_SUCCESS_MSG');
          }
          this.getpartyDetailList();
        },
        (err) => {
          this.spinner.hide();
          this.toaster.showError('TOASTER.ERROR_MSG');
          console.log(err);
        }
      );
  }

  getpartyDetailList() {
    this.spinner.show();
    this.apiService.getApi(API_ENDPOINTS.PARTY_DETAIL, {}, true).subscribe(
      (res) => {
        this.spinner.hide();
        this.partyDetailList = [...res];
        this.partyDetailList.forEach((detail) => {
          const userTypeIndex = _.findIndex(this.cs.userTypeList, {
            Id: detail.userType,
          });
          if (userTypeIndex !== null && userTypeIndex >= 0) {
            detail.userTypeName = this.cs.userTypeList[userTypeIndex].Name;
          }
        });
      },
      (err) => {
        console.log(err);
        this.toaster.showError('TOASTER.ERROR_MSG');
      }
    );
  }

  editDetail(item) {
    this.editId = item._id;
    this.selectedUserTypeId = item.userType;
    this.userName = item.name;
    this.fatherName = item.fatherName;
    this.age = item.age;
    this.cast = item.cast;
    this.address = item.address;
    this.adharNo = item.aadharNo;
    this.panNo = item.panNo;
    this.mobileNo = item.mobileNo;
  }
  deleteDetail(item, index) {
    this.spinner.show();
    const query = { id: item._id };
    this.apiService
      .deleteApi(API_ENDPOINTS.PARTY_DETAIL, query, true)
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.toaster.showSuccess('TOASTER.DELETE_SUCCESS_MSG');
          this.partyDetailList.splice(index, 1);
        },
        (err) => {
          this.spinner.hide();
          this.toaster.showError('TOASTER.ERROR_MSG');
          console.log(err);
        }
      );
  }
}
