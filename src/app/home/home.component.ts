import { CommonService } from './../common.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit {
  formArray: Array<any> = [];
  constructor(
    public cs: CommonService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.formArray = [...this.cs.documentTypeList];
  }
  ngOnInit(): void {}
  ngAfterViewInit() {
    this.route.paramMap.subscribe((res) => {
      if (res.get('type')) {
        this.onClickMenu(res.get('type'));
      } else {
        this.cs.scrollToTop();
      }
    });
  }
  onClickMenu(id) {
    document.getElementById(id).scrollIntoView();
  }
  navigateToForm(path?) {
    if (path) {
      this.router.navigate([`${path}`]);
    }
  }
}
