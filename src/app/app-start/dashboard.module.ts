import { LazyTransModule } from '../lazy-trans.module';
import { SharedModule } from '../shared.module';
import { DashBoardComponent } from './dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashBoardRoutingModule } from './dashboard-routing.module';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [DashBoardComponent, FooterComponent],
  imports: [
    CommonModule,
    DashBoardRoutingModule,
    SharedModule,
    LazyTransModule
  ],
  entryComponents: []
})
export class DashBoardModule { }
