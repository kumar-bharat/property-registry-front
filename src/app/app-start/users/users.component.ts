import { NgxSpinnerService } from 'ngx-spinner';
import { API_ENDPOINTS } from './../../api.endpoint';
import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  userlist: any[];
  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.getUserList();
  }

  getUserList() {
    this.spinner.show();
    this.apiService.getApi(API_ENDPOINTS.ALL_USERS, {}).subscribe(
      (res) => {
        this.spinner.hide();
        if (!_.isEmpty(res)) {
          this.userlist = [...res];
        } else {
          this.userlist = [];
        }
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  // editUsers(){

  // }

  // deleteUsers(){ 
  // }
}
