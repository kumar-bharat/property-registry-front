import { CommonService } from './../common.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
declare const $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashBoardComponent implements OnInit, AfterViewInit {

  constructor(
    public cs: CommonService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
     // Toggle the side navigation
  $('#sidebarToggle, #sidebarToggleTop').on('click', (e) => {
    $('body').toggleClass('sidebar-toggled');
    $('.sidebar').toggleClass('toggled');
    if ($('.sidebar').hasClass('toggled')) {
      $('.sidebar .collapse').collapse('hide');
    }
  });

  }

}
