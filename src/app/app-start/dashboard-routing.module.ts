import { DashBoardComponent } from './dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DashBoardComponent,
    children: [
      {
        path: 'document-detail',
        loadChildren: () =>
          import('../document-type-detail/document-type-detail.module').then(
            (m) => m.DocumentTypeDetailModule
          ),
      },
      {
        path: 'property-detail',
        loadChildren: () =>
          import('../property-detail/property-detail.module').then(
            (m) => m.PropertyDetailModule
          ),
      },
      {
        path: 'party-detail',
        loadChildren: () =>
          import('../party-detail/party-detail.module').then(
            (m) => m.PartyDetailModule
          ),
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./users/users.module').then((m) => m.UsersModule),
      },
      {
        path: 'documents',
        loadChildren: () =>
          import('../document-list/document-list.module').then(
            (m) => m.DocumentListModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashBoardRoutingModule {}
