import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CommonService } from './../../common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  constructor( public cs: CommonService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    let headerOffSet = null
    const headerRef = document.getElementById('mainNav')
    if(headerRef){
      headerOffSet = headerRef.offsetTop
    }
    window.onscroll = () => {
      if (window.pageYOffset > headerOffSet) {
        headerRef.classList.add("fixed-top");
      } else {
        headerRef.classList.remove("fixed-top");
      }
    }
  }

}
