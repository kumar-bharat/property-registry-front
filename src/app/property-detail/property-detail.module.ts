import { LazyTransModule } from './../lazy-trans.module';
import { NgModule } from '@angular/core';
import { PropertyDetailRoutingModule } from './property-detail-routing.module';
import { PropertyDetailComponent } from './property-detail.component';
import { SharedModule } from '../shared.module';


@NgModule({
  declarations: [PropertyDetailComponent],
  imports: [
    PropertyDetailRoutingModule,
    SharedModule,
    LazyTransModule
  ]
})
export class PropertyDetailModule { }
