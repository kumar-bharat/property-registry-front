import { CommonService } from './../common.service';
import { ToasterService } from './../toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from './../api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { API_ENDPOINTS } from '../api.endpoint';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.component.html',
  styleUrls: ['./property-detail.component.scss'],
})
export class PropertyDetailComponent implements OnInit {
  @ViewChild('propertyDetailFrom') propertyDetailFrom: NgForm;
  villageName: string;
  jamabandiSamvat: string;
  namantranSankhya: string;
  patwarMandal: string;
  upTahsil: string;
  tahsil: string;
  district: string;
  propertyMeasurementFields: any[] = [
    {
      id: 0,
      accountNo: null,
      area: null,
      bhoomiKisma: null,
      khasraNo: null,
    },
  ];
  editId: any;
  propertyDetailList: any[];
  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private toaster: ToasterService,
    private cs: CommonService
  ) {}
  addField() {
    const data = {
      accountNo: null,
      area: null,
      bhoomiKisma: null,
      khasraNo: null,
    };
    this.propertyMeasurementFields.push(data);
  }

  removeField(index) {
    this.propertyMeasurementFields.splice(index, 1);
  }

  ngOnInit(): void {
    this.getpropertyDetailList();
  }

  preparePayload() {
    return {
      // documentNo: this.cs.documentNo,
      villageName: this.villageName,
      jamabandiSamvat: this.jamabandiSamvat,
      patwarMandal: this.patwarMandal,
      namantranSankhya: this.namantranSankhya,
      upTahsil: this.upTahsil,
      tahsil: this.tahsil,
      district: this.district,
      propertyMeasurementParameters: _.map(
        this.propertyMeasurementFields,
        (item) => {
          return {
            id: 0,
            accountNo: item.accountNo,
            khasraNo: item.khasraNo,
            area: item.area,
            bhoomiKisma: item.bhoomiKisma,
          };
        }
      ),
    };
  }

  submit() {
    this.spinner.show();
    const query = this.editId ? { id: this.editId } : {};
    this.apiService
      .postApi(
        API_ENDPOINTS.PROPERTY_DETAIL,
        this.preparePayload(),
        true,
        query
      )
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.propertyDetailFrom.resetForm();
          if (this.editId) {
            this.toaster.showSuccess('TOASTER.UPDATE_SUCCESS_MSG');
            this.editId = null;
          } else {
            this.toaster.showSuccess('TOASTER.SAVE_SUCCESS_MSG');
          }
          this.getpropertyDetailList();
        },
        (err) => {
          this.spinner.hide();
          this.toaster.showSuccess('TOASTER.ERROR_MSG');
          console.log(err);
        }
      );
  }

  getpropertyDetailList() {
    this.spinner.show();
    this.apiService.getApi(API_ENDPOINTS.PROPERTY_DETAIL, {}, true).subscribe(
      (res) => {
        this.spinner.hide();
        this.propertyDetailList = [...res];
      },
      (err) => {
        console.log(err);
        this.spinner.hide();
      }
    );
  }

  editProperty(item) {
    this.editId = item._id;
    this.villageName = item.villageName;
    this.jamabandiSamvat = item.jamabandiSamvat;
    this.patwarMandal = item.patwarMandal;
    this.namantranSankhya = item.namantranSankhya;
    this.upTahsil = item.upTahsil;
    this.tahsil = item.tahsil;
    this.district = item.district;
    this.propertyMeasurementFields = _.map(
      item.propertyMeasurementParameters,
      (subItem) => {
        return {
          id: subItem._id,
          accountNo: subItem.accountNo,
          khasraNo: subItem.khasraNo,
          area: subItem.area,
          bhoomiKisma: subItem.bhoomiKisma,
        };
      }
    );
  }
  deleteProperty(item, index) {
    const query = { id: item._id };
    this.spinner.show();
    this.apiService
      .deleteApi(API_ENDPOINTS.PROPERTY_DETAIL, query, true)
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.toaster.showSuccess('TOASTER.DELETE_SUCCESS_MSG');
          this.propertyDetailList.splice(index, 1);
        },
        (err) => {
          this.spinner.hide();
          this.toaster.showError('TOASTER.ERROR_MSG');
          console.log(err);
        }
      );
  }
}
