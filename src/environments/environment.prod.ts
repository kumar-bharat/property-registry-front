export const environment = {
  production: true,
  SERVER_URL: 'https://property-registry.herokuapp.com/api'
};
